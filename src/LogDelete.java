import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LogDelete {

	final static String TARGET_DIRECTORY = "TargetDirectory";
	final static String SUB_DIRECTORY_TYPE = "SubDirectoryType";
	final static String TARGET_SUB_DIRECTORY_LEVEL = "TargetSubDirectoryLevel";
	final static String REMOVE_BLANK_DIRECTORY = "RemoveBlankDirectory";
	final static String DENY_DIRECTORY = "DenyDirectory";
	final static String FILE_DATE_EXPRESSION = "FileDateExpression";
	final static String TARGET_EXTENTION = "TargetExtention";
	final static String LOG_RETENTION_PERIOD_DAY = "LogRetentionPeriodDay";
	final static String VALUE = "value";

	static List<String> denyDirectoryList = null;
	static List<String> targetExtentionList = null;
	static List<String> fileDateExpressionList = null;
	static String targetDirectory = null;
	static String logRetentionPeriodDay = null;
	static String removeBlankDirectory = null;

	static List<String> removeTargetBlankDirectory = null;

    static boolean isDebug = false;
    static boolean isTest = false;
	
	/**
	 * @param args
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws XPathExpressionException 
	 */
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
        System.out.println("Start Log Delete..");
        
        // 디버그 모드
        if (args != null && args.length > 1 && args[1].equals("debug")) isDebug = true;
        
        // Config 파일 위치
        if (isTest == false 
        		&& (args == null || args.length < 1)) {
        	System.err.println("config.xml 파일의 위치가 설정되지 않았습니다.");
        	System.out.println("End Log Delete..");
        	System.exit(-1);
        }
        
        // 설정파일을 읽어 들인다.
        File config = null;
        if (isTest) {
            config = new File("config.xml"); // 테스트용
            System.err.println("프로그램이 테스트 모드로 수행 중입니다.");
        } else {
            config = new File(args[0]);
        }
 
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(config);
        XPath xpath = XPathFactory.newInstance().newXPath();
        
        removeTargetBlankDirectory = new ArrayList<String>();
 
        NodeList targetDirList = (NodeList) xpath.compile("//Config/TargetDirectory").evaluate(document, XPathConstants.NODESET);
        
        mainLabel:
        for (int idx = 0; idx < targetDirList.getLength(); idx++){
        	String value = checkNull(targetDirList.item(idx).getAttributes().getNamedItem(VALUE).getTextContent());
        	String subDirectoryType = checkNull(targetDirList.item(idx).getAttributes().getNamedItem(SUB_DIRECTORY_TYPE).getTextContent());
        	
        	// TargetDirectory를 전역 변수에 저장한다.
        	targetDirectory = value;
        	
        	if (isDebug) System.out.println(TARGET_DIRECTORY + ", value : " + value + ", subDirectoryType : " + subDirectoryType);

        	// TargetDirectory Element
        	Element element = (Element) targetDirList.item(idx);
        	
        	// 재사용 객체
        	NodeList nodeList = null;

        	// TargetSubDirectoryLevelNodeList
        	NodeList targetSubDirectoryLevelNodeList = null;
        	// RemoveBlankDirectoryNodeList
        	NodeList removeBlankDirectoryNodeList = null;

        	// TargetSubDirectoryLevel
        	String targetSubDirectoryLevel = "";
        	targetSubDirectoryLevelNodeList = element.getElementsByTagName(TARGET_SUB_DIRECTORY_LEVEL);
        	for (int k = 0; k < targetSubDirectoryLevelNodeList.getLength(); k++) {
        		targetSubDirectoryLevel = checkNull(targetSubDirectoryLevelNodeList.item(k).getAttributes().getNamedItem(VALUE).getTextContent());
            	if (isDebug) System.out.println(TARGET_SUB_DIRECTORY_LEVEL + ", value : " + targetSubDirectoryLevel);
        	}
        	
        	// RemoveBlankDirectory
        	removeBlankDirectory = "";
        	removeBlankDirectoryNodeList = element.getElementsByTagName(REMOVE_BLANK_DIRECTORY);
        	for (int k = 0; k < removeBlankDirectoryNodeList.getLength(); k++) {
            	removeBlankDirectory = checkNull(removeBlankDirectoryNodeList.item(k).getAttributes().getNamedItem(VALUE).getTextContent());
            	if (isDebug) System.out.println(REMOVE_BLANK_DIRECTORY + ", value : " + removeBlankDirectory);
        	}
        	
        	// DenyDirectory
        	denyDirectoryList = new ArrayList<String>();
        	nodeList = element.getElementsByTagName(DENY_DIRECTORY);
        	for (int k = 0; k < nodeList.getLength(); k++) {

            	Element subElement = (Element) nodeList.item(k);

            	// DenyDirectory/value
            	NodeList subNodeList = subElement.getElementsByTagName(VALUE);
            	for (int m = 0; m < subNodeList.getLength(); m++) {
            		String childValue = checkNull(subNodeList.item(m).getTextContent());
            		denyDirectoryList.add(childValue);
            		if (isDebug) System.out.println(DENY_DIRECTORY + "/" + VALUE + ", value : " + childValue);
            	}
        	}
        	
        	// FileDateExpression
        	fileDateExpressionList = new ArrayList<String>();
        	nodeList = element.getElementsByTagName(FILE_DATE_EXPRESSION);
        	for (int k = 0; k < nodeList.getLength(); k++) {

            	Element subElement = (Element) nodeList.item(k);

            	// FileDateExpression/value
            	NodeList subNodeList = subElement.getElementsByTagName(VALUE);
            	for (int m = 0; m < subNodeList.getLength(); m++) {
            		String childValue = checkNull(subNodeList.item(m).getTextContent());
            		fileDateExpressionList.add(childValue);
            		if (isDebug) System.out.println(FILE_DATE_EXPRESSION + "/" + VALUE + ", value : " + childValue);
            	}
        	}

        	// TargetExtention
        	targetExtentionList = new ArrayList<String>();
        	nodeList = element.getElementsByTagName(TARGET_EXTENTION);
        	for (int k = 0; k < nodeList.getLength(); k++) {

            	Element subElement = (Element) nodeList.item(k);

            	// TargetExtention/value
            	NodeList subNodeList = subElement.getElementsByTagName(VALUE);
            	for (int m = 0; m < subNodeList.getLength(); m++) {
            		String childValue = checkNull(subNodeList.item(m).getTextContent());
            		targetExtentionList.add(childValue);
            		if (isDebug) System.out.println(TARGET_EXTENTION + "/" + VALUE + ", value : " + childValue);
            	}
        	}

        	// LogRetentionPeriodDay
        	logRetentionPeriodDay = "";
        	nodeList = element.getElementsByTagName(LOG_RETENTION_PERIOD_DAY);
        	for (int k = 0; k < nodeList.getLength(); k++) {
        		logRetentionPeriodDay = checkNull(nodeList.item(k).getAttributes().getNamedItem(VALUE).getTextContent());
            	if (isDebug) System.out.println(LOG_RETENTION_PERIOD_DAY + ", value : " + logRetentionPeriodDay);
        	}
        	
        	// 파일삭제 처리 시작
        	File targetDir = new File(value);
        	
        	File[] flist = targetDir.listFiles();
        	
        	if (flist == null) {
        		System.err.println("존재하지 않는 디렉토리 입니다. [" + value + "]");
        		continue;
        	}
        	
        	int targetLevel = 0;
    		try {
    			targetLevel = Integer.parseInt(targetSubDirectoryLevel);
    		} catch (NumberFormatException e) {
    			targetLevel = 0;
    		}
        	
        	for (File f : flist) {
        		if (isDebug) System.out.println(f.getAbsolutePath());
        		
        		// 서브 디렉토리 타입인 경우에는 해당 수만큼 하위 디렉토리를 뒤지면서 재귀호출하면서 파일들을 삭제처리한다.
        		if (subDirectoryType.equals("y")) {
        			int level = 1;
        			findSubDirectory(f, targetLevel, level);
        		} else {
        			// 서브 디렉토리 타입이 아닌 경우에는 TargetSubDirectoryLevel, RemoveBlankDirectory는 설정하지 않도록 처리를 종료한다.
        			if (targetSubDirectoryLevelNodeList != null && targetSubDirectoryLevelNodeList.getLength() > 0) {
        				System.err.println(SUB_DIRECTORY_TYPE + "이 n인 경우에는 " + TARGET_SUB_DIRECTORY_LEVEL + "을 지정할 수 없습니다.");
        				continue mainLabel;
        			}
        			if (removeBlankDirectoryNodeList != null && removeBlankDirectoryNodeList.getLength() > 0) {
        				System.err.println(SUB_DIRECTORY_TYPE + "이 n인 경우에는 " + REMOVE_BLANK_DIRECTORY + "을 지정할 수 없습니다.");
        				continue mainLabel;
        			}
        			
        			if (f.isFile()
        					&& (isDenyDirectory(f.getAbsolutePath()) == false && isTargetExtention(f.getAbsolutePath()) == true && isDeleteConditionFile(f.getAbsolutePath()) == true)) {
        				// 삭제대상 파일을 지정한다.
        				System.out.println("삭제 대상 파일: " + f.getAbsolutePath() + ", result: " + f.delete());
        			}
        		}
        	}
        	// END 파일삭제 처리
        	
        	// 비어있는 디렉토리 삭제 처리
        	if (removeBlankDirectory.equals("y")) {
        		removeBlankDirectory();
        	}
        }
        
        System.out.println("End Log Delete..");
	}

	private static void findSubDirectory(File file, int targetLevel, int level) {
		if (isDebug) System.out.println("targetLevel : " + targetLevel + ", cnt : " + level);
		int subLevel = level + 1;
		File[] flist = file.listFiles();
		if (flist == null) return;
		for (File f : flist) {
			if (f.isDirectory()) {
				// 설정파일의 대상 타겟 레벨을 넘는 경우에는 건너뛴다.
				if (targetLevel < subLevel) {
					continue;
				} else {
					findSubDirectory(f, targetLevel, subLevel);
				}
			} else if (f.isFile()
					&& (isDenyDirectory(f.getAbsolutePath()) == false && isTargetExtention(f.getAbsolutePath()) == true && isDeleteConditionFile(f.getAbsolutePath()) == true)) {
				// 삭제대상 파일을 지정한다.
				System.out.println("삭제 대상 파일: " + f.getAbsolutePath() + ", result: " + f.delete());
				
				// SubDirectoryType이 y이고 RemoveBlankDirectory가 y 인 경우에는 비어있는 디렉토리를 삭제한다.
				if (removeBlankDirectory.equals("y")) {
					saveCheckTargetDirectory(f.getAbsoluteFile().getParentFile().toString());
				}
    		}
		}
	}
	
	private static void removeBlankDirectory() {
		
		// 객체를 재생성 한다.
		List<String> list = new ArrayList<String>(removeTargetBlankDirectory);
		
		// 중복제거
		List<String> uniqueList = new ArrayList<String>(new HashSet<String>(list));
		
		// 하위 디렉토리부터 삭제하기 위해서 디렉토리명들을 정렬한다.
		Collections.sort(uniqueList);
		
		// 하위 디렉토리부터 삭제하기 위해서 디렉토리명들을 다시 역순으로 정렬한다.
		Collections.reverse(uniqueList);
		
		for (String dirPath : uniqueList) {
			File f = new File(dirPath);
			boolean result = f.delete();
			System.out.println("비어있는 경우 삭제되는 디렉토리: " + dirPath + ", result: " + result);
		}
	}
	
	private static void saveCheckTargetDirectory(String currentDirectory) {

		// 현재의 디렉토리가 TargetDirectory의 바로 하위 디렉토리이면 삭제하지 않는다.(로그 모듈 디렉토리는 남긴다.)
		// 현재의 디렉토리가 TargetDirectory 보다 짧으면 더 이상 상위로 올라가지 않는다.
		// 현재의 디렉토리가 TargetDirectory와 같으면 더 이상 상위로 올라가지 않는다.
		String pattern = Pattern.quote(System.getProperty("file.separator"));

		String[] targetDirectoryLevel = targetDirectory.split(pattern);
		String[] currentDirectoryLevel = currentDirectory.split(pattern);
		
		if (targetDirectoryLevel == null) targetDirectoryLevel = new String[]{""};
		if (currentDirectoryLevel == null) currentDirectoryLevel = new String[]{""};
		
		if (targetDirectoryLevel.length < ( currentDirectoryLevel.length - 1 ) == false
				|| targetDirectory.length() < currentDirectory.length() == false
				|| targetDirectory.equals(currentDirectory)) {
			return;
		}

		// 후 처리를 위해서 링크드맵에 삭제대상 디렉토리를 저장한다.
		removeTargetBlankDirectory.add(currentDirectory);
		
		saveCheckTargetDirectory((new File(currentDirectory)).getAbsoluteFile().getParentFile().toString());
	}
	
	private static boolean isDenyDirectory(String absoluteFilePath) {
		if (absoluteFilePath == null) return false;
		if (denyDirectoryList == null) return false;
		for (String denyDirectory : denyDirectoryList) {
			if (absoluteFilePath.indexOf(System.getProperty("file.separator") + denyDirectory + System.getProperty("file.separator")) < 0) {
				return false;
			} else {
				return true;
			}
		}
		return false;
	}

	private static boolean isTargetExtention(String absoluteFilePath) {
		if (absoluteFilePath == null) return false;
		if (targetExtentionList == null) return false;
		for (String targetExtention : targetExtentionList) {
			if (absoluteFilePath.endsWith(targetExtention)) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
	
	private static boolean isDeleteConditionFile(String absoluteFilePath) {
		if (absoluteFilePath == null) return false;
		if (fileDateExpressionList == null) return false;
		if (logRetentionPeriodDay == null) return false;
		
		int logRetentionPeriodDayNum = 0;
		try {
			logRetentionPeriodDayNum = Integer.parseInt(logRetentionPeriodDay);
		} catch (NumberFormatException e) { 
			return false;
		}
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, logRetentionPeriodDayNum * -1);
		Date targetDay = cal.getTime();

		Date fileDate = null;

		// 파일명에서 맨끝을 잘라서 저장한다.
		String fileName = absoluteFilePath.substring(absoluteFilePath.lastIndexOf(System.getProperty("file.separator")));
		if (fileName == null) {
			fileName = "";
		} else {
			fileName = fileName.replace(System.getProperty("file.separator"), "");
		}
		
		for (String fileDateExpression : fileDateExpressionList) {
			
			// 파일 날짜를 매핑시켜 본다.
			String tmpFileName = "";//fileName.substring(0, fileDateExpression.length());

			Pattern pattern = Pattern.compile("[0-9]{4}.?[0-9]{2}.?[0-9]{2}");
			Matcher mc = pattern.matcher(fileName);
			if (mc.find()) {
				tmpFileName = mc.group();
				if (isDebug) System.out.println("fileName: " + fileName + ", matchFileName: " + tmpFileName);
				/*
				for (int i = 0; i <= mc.groupCount(); i++) {
					System.out.println("group(" + i + ") = " + mc.group(i));
				}
				*/
			}
			
			SimpleDateFormat formatter = new SimpleDateFormat(fileDateExpression, Locale.getDefault());
			
			// 상세한 해석을 하도록 설정한다.(중요)
			formatter.setLenient(false);
			
			try {
				fileDate = formatter.parse(tmpFileName);
				break; // 에러 없이 잘 변경 되었으면 매칭된 것으로 본다.
			} catch (ParseException e) { }
		}
		
		if (isDebug) System.out.println("targetDay: " + targetDay + ", fileDate: " + fileDate);
		
		// 해당 파일이 TargetDay 보다 큰 날짜인 경우에는 삭제한다.
		int compare = targetDay.compareTo(fileDate);
		
		if (compare > 0) {
			return true;
		} else {
			return false;
		}
	}
	
    private static String checkNull(String str) {
 
        if (str == null || str.trim().equals("")) {
            return "";
        } else {
            return str.trim();
        }
    }
}
