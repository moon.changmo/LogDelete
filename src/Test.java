import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;


public class Test {

	public static void main(String[] args) {

		List<String> removeTargetBlankDirectory = new ArrayList<String>();
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_bbs\\2015\\06");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_terms\\2015\\06");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_kms\\2015\\06");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\cmanager\\2015\\08");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\cmanager\\2015\\07");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\cmanager\\2015\\09");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_edu\\2015\\06");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\cmanager\\2015\\06");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_edu");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_kms");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_bbs");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_terms");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\ark\\2015\\06");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_edu\\2015");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_terms\\2015");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_kms\\2015");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\isc\\2015");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\cmanager\\2015");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\ark\\2015");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_bbs\\2015");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\isc\\2015\\08");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\isc\\2015\\09");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\isc\\2015\\06");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\isc\\2015\\07");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_bbs\\2015\\06");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_terms\\2015\\06");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_kms\\2015\\06");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\cmanager\\2015\\08");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\cmanager\\2015\\07");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\cmanager\\2015\\09");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_edu\\2015\\06");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\cmanager\\2015\\06");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_edu");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_kms");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_bbs");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_terms");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\ark\\2015\\06");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_edu\\2015");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_terms\\2015");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_kms\\2015");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\isc\\2015");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\cmanager\\2015");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\ark\\2015");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\indexer\\sample_bbs\\2015");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\isc\\2015\\08");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\isc\\2015\\09");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\isc\\2015\\06");
		removeTargetBlankDirectory.add("C:\\wisenut\\sf-1v5.3\\log\\isc\\2015\\07");
		
		// 중복제거
		List<String> uniqueList = new ArrayList<String>(new HashSet<String>(removeTargetBlankDirectory));
		
		// 하위 디렉토리부터 삭제하기 위해서 디렉토리명들을 정렬한다.
		Collections.sort(uniqueList);

		// 하위 디렉토리부터 삭제하기 위해서 디렉토리명들을 다시 역순으로 정렬한다.
		Collections.reverse(uniqueList);
		
		for (String dirPath : uniqueList) {
			File f = new File(dirPath);
			boolean result = f.delete();
			System.out.println("dirPath: " + dirPath + ", result: " + result);
		}
		
	}
}
